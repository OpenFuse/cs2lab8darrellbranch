package edu.westga.cs1302.statedata.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class States.
 * 
 * @author Darrell Branch
 */
public class States {

	/** The states. */
	private Map<String, StateData> states;
	
	/**
	 * Instantiates a new states collection.
	 */
	public States() {
		this.states = new HashMap<String, StateData>();
	}
	
	/**
	 * Clear.
	 */
	public void clear() {
		this.states.clear();
	}
	
	/**
	 * Gets the.
	 *
	 * @param key the key
	 * @return the state data
	 */
	public StateData get(Object key) {
		return this.states.get(key);
	}
	
	/**
	 * Contains key.
	 *
	 * @param key the key
	 * @return true, if successful
	 */
	public boolean containsKey(Object key) {
		return this.states.containsKey(key);
	}
	
	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return this.isEmpty();
	}
	
	/**
	 * Put.
	 *
	 * @param key the key
	 * @param value the value
	 * @return the state data
	 */
	public StateData put(String key, StateData value) {
		return this.states.put(key, value);
	}
	
	/**
	 * Replace all.
	 *
	 * @param collection the collection
	 */
//	public void replaceAll(Collection<Object> collection) {
//		this.states.replaceAll(collection);
//	}
	
	/**
	 * Removes the.
	 *
	 * @param key the key
	 * @return the state data
	 */
	public StateData remove(Object key) {
		return this.states.remove(key);
	}
	
	/**
	 * Size.
	 *
	 * @return the int
	 */
	public int size() {
		return this.states.size();
	}
	
	/**
	 * Values.
	 *
	 * @return the collection
	 */
	public Collection<StateData> values() {
		return this.states.values();
	}
}
