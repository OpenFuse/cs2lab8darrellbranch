package edu.westga.cs1302.statedata.view;

import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.viewmodel.StateDataViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;
import javafx.util.converter.NumberStringConverter;

/**
 * The Class StateDataCodeBehind.
 * 
 * @author CS1302
 * @version Lab 8
 */
/**
 * The Class CodeBehind.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class StateDataGuiCodeBehind {

	private StateDataViewModel viewmodel;

	@FXML
	private AnchorPane guiPane;

	@FXML
	private TextField stateNameTextField;

	@FXML
	private TextField populationTextField;

	@FXML
	private TextField populationChangeTextField;

	@FXML
	private ListView<StateData> statesListView;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button searchButton;
	
	@FXML
	private Button updateButton;
	
	/**
	 * Instantiates a new code behind.
	 */
	public StateDataGuiCodeBehind() {
		this.viewmodel = new StateDataViewModel();
	}

	@FXML
	void initialize() {
		this.bindToViewModel();
		this.setListenersForListView();
	}

	@FXML
    void handleAddState(ActionEvent event) {
		try {
			if (!this.viewmodel.addState()) {
				this.showAlert("Add error", "ERROR: state was not added");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Add error", "ERROR: couldn't add the state due to " + ex.getMessage());
		}
    }

	@FXML
	void handleUpdateState(ActionEvent event) {
		try {
			if (!this.viewmodel.updateState()) {
				this.showAlert("Update error", "ERROR: No such a state");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Update error",	"ERROR: couldn't update the state due to " + ex.getMessage());
		}
	}

    @FXML
    void handleSearchForState(ActionEvent event) {
    	try {
			if (!this.viewmodel.searchForState()) {
				this.showAlert("Search error", "ERROR: No such state");
			}
		} catch (IllegalArgumentException | NullPointerException ex) {
			this.showAlert("Search error", "ERROR: couldn't find the state due to " + ex.getMessage());
		}
    }
    
    private void setListenersForListView() {
		this.statesListView.getSelectionModel().selectedItemProperty().addListener((observable, oldState, newState) -> {
			if (newState != null) {
				this.stateNameTextField.setText(this.viewmodel.stateNameProperty().get());
				this.populationTextField.setText(Integer.toString(this.viewmodel.populationProperty().get()));
				this.populationChangeTextField.setText(Integer.toString(this.viewmodel.populationChangeProperty().get()));
			}
		});
	}

	private void showAlert(String title, String content) {
		Alert alert = new Alert(AlertType.ERROR);
		Window owner = this.guiPane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(content);
		alert.showAndWait();
	}
	
	private void bindToViewModel() {
		this.stateNameTextField.textProperty().bindBidirectional(this.viewmodel.stateNameProperty());
		this.populationTextField.textProperty().bindBidirectional(this.viewmodel.populationProperty(), new NumberStringConverter());
		this.populationChangeTextField.textProperty().bindBidirectional(this.viewmodel.populationChangeProperty(), new NumberStringConverter());
		this.statesListView.itemsProperty().bind(this.viewmodel.statesProperty());
		
		BooleanBinding propertiesFilled = Bindings.and(this.stateNameTextField.textProperty().isEmpty(),
													   Bindings.and(this.populationTextField.textProperty().isEmpty(),
													   this.populationChangeTextField.textProperty().isEmpty()));
		
		this.addButton.disableProperty().bind(propertiesFilled);
		
		BooleanBinding enableSearch = Bindings.or(this.stateNameTextField.textProperty().isEmpty(), this.viewmodel.statesDataProperty().isEqualTo(0));
		
		this.searchButton.disableProperty().bind(enableSearch);
		
		BooleanBinding enableUpdateButon = Bindings.or(propertiesFilled, this.viewmodel.statesDataProperty().isEqualTo(0));
		
		this.updateButton.disableProperty().bind(enableUpdateButon);
	}
}