package edu.westga.cs1302.statedata.resources;

/**
 * Defines all the exception messages
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class ExceptionMessages {
	public static final String NULL_NAME = "name cannot be null.";
	public static final String EMPTY_NAME = "name cannot be empty.";
	public static final String NEGATIVE_POPULATION = "population cannot be negative.";
}
