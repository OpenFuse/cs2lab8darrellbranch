package edu.westga.cs1302.statedata.viewmodel;

import edu.westga.cs1302.statedata.model.StateData;
import edu.westga.cs1302.statedata.model.States;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * The Class SateDataViewModel.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class StateDataViewModel {

	private final StringProperty stateNameProperty;
	private final IntegerProperty populationProperty;
	private final IntegerProperty populationChangeProperty;
	private final ListProperty<StateData> statesProperty;
	private States statesCollection;
	private IntegerProperty statesDataProperty;

	/**
	 * Instantiates a new password manager gui view model.
	 */
	public StateDataViewModel() {
		this.stateNameProperty = new SimpleStringProperty();
		this.populationProperty = new SimpleIntegerProperty();
		this.populationChangeProperty = new SimpleIntegerProperty();
		this.statesProperty = new SimpleListProperty<StateData>();
		this.statesCollection = new States();
		this.statesDataProperty = new SimpleIntegerProperty();
	}
	
	/**
	 * States data property.
	 *
	 * @return the integer property
	 */
	public IntegerProperty statesDataProperty() {
		this.statesDataProperty.set(this.statesCollection.size());
		return this.statesDataProperty;
	}

	/**
	 * Gets the state name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the state name property
	 */
	public StringProperty stateNameProperty() {
		return this.stateNameProperty;
	}

	/**
	 * Gets the population property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population property
	 */
	public IntegerProperty populationProperty() {
		return this.populationProperty;
	}

	/**
	 * Gets the population change property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the population change property
	 */
	public IntegerProperty populationChangeProperty() {
		return this.populationChangeProperty;
	}
	
	/**
	 * Gets the states property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the states property
	 */
	public ListProperty<StateData> statesProperty() {
		return this.statesProperty;
	}

	/**
	 * Adds the state.
	 * 
	 * @precondition none
	 * @postcondition this.statesCollection.size()prev == this.statesCollection.size() - 1
	 * 				  stateNameProperty.get() == ""; populationProperty.get() == 0;
	 * 				  populationChangeProperty.get() == 0;
	 *
	 * @return true, if successful
	 */
	public boolean addState() {
		StateData newState;
		try {
			newState = new StateData(this.stateNameProperty.get(), this.populationProperty.get(), this.populationChangeProperty.get());
		} catch (Exception e) {
			return false;
		}
		this.statesCollection.put(newState.getName(), newState);
		
		this.stateNameProperty.set("");
		this.populationProperty.setValue(0);
		this.populationChangeProperty.setValue(0);
		
		this.statesProperty.set((ObservableList<StateData>) this.statesCollection.values());

		return true;
	}

	/**
	 * Update state.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return true, if successful
	 */
	public boolean updateState() {
		if (this.statesCollection.containsKey(this.stateNameProperty.get())) {
			StateData updatedState = this.statesCollection.get(this.stateNameProperty.get());
			
			updatedState.setPopulation(this.populationProperty.get());
			updatedState.setPopulationChange(this.populationChangeProperty.get());
			
			this.statesProperty.set((ObservableList<StateData>) this.statesCollection.values());
			
			return true;
		}

		return false;
	}
	
	/**
	 * Search for a state.
	 *
	 * @precondition none
	 * @postcondition populationProperty.get() == retrievedState.getPopulation;
	 * 	   			  populationChangeProperty.get() == retrievedState.getPopulationChange();
	 *
	 * @return true, if successful
	 */
	public boolean searchForState() {
		if (this.statesCollection.containsKey(this.stateNameProperty.get())) {
			StateData retrievedState = this.statesCollection.get(this.stateNameProperty.get());
			this.populationProperty.set(retrievedState.getPopulation());
			this.populationChangeProperty.set(retrievedState.getPopulationChange());
			return true;
		}

		return false;
	}

}
